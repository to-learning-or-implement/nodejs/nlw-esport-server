# Back-end

## Entidades

### Game

id
title
bannerUrl

CDN (Amazon S3) (Content Delivery Network) => url da imagem

### Ad

id
gameId
name
yearsPlaying
discord
weekDays
hourStart
hourEnd
useVoiceChannel
createdAt

1:30h -> 90 min
R$ 179,89 -> 17989 (salva no banco o valor multiplicado por 100)

Datas (fuso horário / formatos diferentes)
Pontos flutuantes

## Casos de uso

- Listagem de games com contagem de anúncios
- Criação de novo anúncio
- Listagem de anúncios por game
- Buscar discord pelo ID do anúncio
