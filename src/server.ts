import express from "express";
import cors from "cors";

import { PrismaClient } from "@prisma/client";
import { convertHourStringToMinutes } from "./utils/convert-hour-string-to-minutes";
import { convertMinutesToHourString } from "./utils/convert-minutes-to-hour-string";

/*
 * HTTP methods / API RESTful / HTTP Codes
 * Os métodos são uma convenção
 *
 * GET - Realiza leitura no backend busca informação
 * POST - Cria uma entidade / recurso
 * PUT - Edita uma entidade praticamente por completo
 * PATCH - Edita uma informação específica em uma entidade (e.g. - boolean de
 *                                                          receber notificação)
 * DELETE - Remove uma entidade do backend
 */

const app = express();
const prisma = new PrismaClient(); // realiza conexão com o bd automaticamente

app.use(express.json()); // Permite que body entenda informações em JSON
app.use(cors()); // Configura quais dominios você quer permitir requisições

/**
 * GET - List Games
 */
app.get("/games", async (_, response) => {
  const games = await prisma.game.findMany({
    include: {
      _count: {
        select: {
          ads: true,
        },
      },
    },
  });

  return response.json(games);
});

/**
 * POST - Create Ad
 */
app.post("/games/:id/ads", async (request, response) => {
  const gameId = request.params.id;
  const body: any = request.body;

  const ad = await prisma.ad.create({
    data: {
      gameId,
      name: body.name,
      yearsPlaying: body.yearsPlaying,
      discord: body.discord,
      weekDays: body.weekDays.join(","),
      hourStart: convertHourStringToMinutes(body.hourStart),
      hourEnd: convertHourStringToMinutes(body.hourEnd),
      useVoiceChannel: body.useVoiceChannel,
    },
  });

  return response.status(201).json(ad);
});

/**
 * GET - List Ads by Game
 */
app.get("/games/:id/ads", async (request, response) => {
  const gameId = request.params.id;
  const ads = await prisma.ad.findMany({
    // SELECT - seleciona todos os campos desejados com 'true'
    select: {
      id: true,
      name: true,
      weekDays: true,
      hourStart: true,
      hourEnd: true,
      useVoiceChannel: true,
      yearsPlaying: true,
    },
    where: {
      gameId,
    },
    orderBy: {
      createdAt: "desc",
    },
  });

  return response.json(
    /**
     * Percorro cada anúncio p/ retornar um array com o dia da semana ao invés
     * de uma string
     */
    ads.map((ad: any) => {
      return {
        ...ad,
        weekDays: ad.weekDays.split(","), // Formatação antes de retornar
        hourStart: convertMinutesToHourString(ad.hourStart),
        hourEnd: convertMinutesToHourString(ad.hourEnd),
      };
    })
  );
});

/**
 * GET - Discord by Ad
 */
app.get("/ads/:id/discord", async (request, response) => {
  const adId = request.params.id;
  const ad = await prisma.ad.findUniqueOrThrow({
    select: {
      discord: true,
    },
    where: {
      id: adId,
    },
  });

  return response.json({
    discord: ad.discord,
  });
});

app.listen(3333);
